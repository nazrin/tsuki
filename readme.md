# Tsuki: WIP Image Board

## Configuration

* `cp config.def.lua config.lua`
Then edit `config.lua`

**Note to set proxied option correctly as described in config.def.lua**

## Prerequisites

### [Luvit](https://luvit.io/)

* `curl -L https://github.com/luvit/lit/raw/master/get-lit.sh | sh`

This will create `luvit` in your PWD. Cd to `$HOME/.local/bin` or `/usr/local/bin` prior if you want

### [Luarocks](https://luarocks.org/)

* `pacman -S luarocks`
* `apt install luarocks`
* Etc

### Modules

Run `sudo make` or `make LRFLAGS='--lua-version=5.1 --local'` to install the Luarocks and compile C packages automatically 

Run `make clibs` to only compile the C libs

## Running

* `luvit .`
* `scripts/run`

If [entr](https://github.com/eradman/entr) is installed `run` will automatically restart when source files change

