CC     = cc
CFLAGS = -shared -fPIC `pkg-config luajit --cflags 2>/dev/null || pkg-config lua5.1 --cflags`
LFLAGS =

LR      = luarocks
LRFLAGS = --lua-version=5.1

all:   clibs luarocks
clibs: src/nilunpack.so
libs:  luarocks

src/nilunpack.so: src/nilunpack.c
	cc $< -o $@ $(CFLAGS) $(LFLAGS)

luarocks:
	@for rock in luadbi lua-messagepack luafilesystem luadbi-sqlite3 lil openssh-hash ; do \
		$(LR) show $$rock $(LRFLAGS) >/dev/null ;\
		if [ $$? -gt 0 ] ; then \
			echo Installing $$rock ;\
			$(LR) install $$rock $(LRFLAGS) ;\
		fi \
	done

local:
	$(MAKE) LRFLAGS='--lua-version=5.1 --local'

clean:
	rm -fv -- src/nilunpack.so
