local config = {
	proxied = false, -- XXX Important! Set to true and set X-Forwarded-For header in Nginx if using proxy_pass
	name = "Tsuki",
	desc = "Tsuki is an open source imageboard written in lua.",
	maxFileNum = 5,
	maxFileSize = 1024*1024*5,
	publicRoot = "./public/",
	bind = {
		addr = "0.0.0.0",
		port = 1337
	},
	db = { "SQLite3", "./db.sqlite" },
	filePath = "./public/files/",
	saltPath = "./passSalt.bin",
	thumbnails = {
		format = ".webp",
		size = { 300, 300 },
		ops = { quality = 0.8, speed = 0.5 }
	},
	defaultUserName = "Anonymous",
	defaultSession = {
		name = "Anonymous",
		role = "anon",
		priv = 0
	},
	divideBytes = 1024,
	bytesArray = {"B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"},
	publicMaxAge = 3600000,
}

config.boards = {
	{
		tag = "a",
		name = "Anime",
		ids = true
	},
	{
		tag = "g",
		name = "Technology",
		ids = true
	}
}

-- Set timezone for client
local tzHandle = io.popen("readlink /etc/localtime")
local tzResult = tzHandle:read()
tzHandle:close()
local tzStart, tzEnd, serverTimezone = tzResult:find("([^/]+/[^/]+)$")
config.serverTimezone = serverTimezone

return config
