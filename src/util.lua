local config = require("./config")
local msgpack = require("MessagePack")

local util = {}

function util.split(self, d)
	local arr = {}
	local i = 1
	for m in self:gmatch("[^"..d.."]+") do
		arr[i] = m
		i = i + 1
	end
	return arr
end

local mimes = {
	webp = "image/webp", -- Just in case some distro does not have it (jewbuntu)
	default = "application/octet-stream",
}

for line in io.open("/etc/mime.types"):lines() do
	local fields = util.split(line, "\t")
	if fields[2] then
		local mimeType = fields[1]
		local fileTypes = util.split(fields[2], " ")
		for _,t in ipairs(fileTypes) do
			mimes[t] = mimeType
		end
	end
end

function util.getType(path)
  return mimes[path:lower():match("[^.]*$")] or mimes.default
end

function util.getBody(req, res, cb)
	local chunks = {}
	local length = 0
	local targetLength = tonumber(req.headers["Content-Length"])
	if not (targetLength and targetLength >= 0) then return res:error(400, "Invalid Content-Length") end
	req:on("data", function(chunk)
		length = length + #chunk
		if length > 55555555 then -- XXX
			return res:error(400, "Too big")
		end
		table.insert(chunks, chunk)
		if length == targetLength then
			local data = table.concat(chunks)
			cb(data)
		end
	end)
end

function util.getJson(req, res, cb)
	util.getBody(req, res, function(data)
		local ok, tbl
		if req.headers["Content-Type"] == "application/json" then
			ok, tbl = pcall(json.decode, data)
		elseif req.headers["Content-Type"] == "application/msgpack" then
			ok, tbl = pcall(msgpack.unpack, data)
		else
			return res:error(400, "Unknown encoding")
		end
		if not (ok and type(tbl) == "table") then return res:error(400, "Error decoding") end
		cb(tbl)
	end)
end

function util.template(str, tbl)
	return str:gsub("{{(%w-)}}", function(m)
		return tbl[m] or ("{{%s}}"):format(m)
	end)
end

function util.htmlEscape(str)
	if(str == nil) then return nil end
	return str:gsub("[\">/<'&]", {
			["&"] = "&amp;",
			["<"] = "&lt;",
			[">"] = "&gt;",
			['"'] = "&quot;",
			["'"] = "&#39;",
			["/"] = "&#47;"
		})
end

-- 1000KB looks ugly and takes up space
-- dividing by 1000 instead of 1024 takes care of that
function util.humanReadableBytes(bytes, divide)
	local divideBytes, bytesArray = config.divideBytes, config.bytesArray
	if(divide == nil) then
		divide = divideBytes
	end
	
	local counter = 1
	while(bytes > divideBytes) do
		bytes = bytes / divideBytes
		counter = counter + 1
	end
	
	return tostring(util.round(bytes, 2)) .. " " .. bytesArray[counter]
end

function util.round(num, numDecimalPlaces)
  local mult = 10^(numDecimalPlaces or 0)
  return math.floor(num * mult + 0.5) / mult
end

function util.toHex(blob)
	local h = {}
	for i=1,#blob do
		h[i] = string.format("%02x", blob:byte(i))
	end
	return table.concat(h, "")
end

local randomFile = io.open("/dev/urandom", "r")
function util.getRandomBytes(n)
	return randomFile:read(n)
end
function util.getRandomHex(n)
	return util.toHex(randomFile:read(n))
end

return util

