local util = require("./src/util")

local expect = {}

function expect.emap(wl)
	return function(map)
		if type(map) ~= "table" then return false end
		for k,v in pairs(map) do -- Check for unknown values
			if not wl[k] then return false end
		end
		for k,check in pairs(wl) do -- Verify each entry
			if not check(map[k]) then return false end
		end
		return true
	end
end
function expect.convmap(wl)
	return function(map)
		if type(map) ~= "table" then return false end
		for k,v in pairs(map) do -- Check for unknown values
			if not wl[k] then return false end
		end
		for k,check in pairs(wl) do -- Verify each entry
			local v = check[1](map[k])
			if not check[2](v) then return false end
			map[k] = v
		end
		return true
	end
end
function expect.earr(min, max, t)
	local tint = expect.eint(min or 1, max or math.huge)
	return function(arr)
		if type(arr) ~= "table" then return false end
		local l = #arr
		if not tint(l) then return false end
		for k,v in pairs(arr) do -- Check for unknown values
			if not tint(k) then return false end
		end
		for k,v in ipairs(arr) do -- Verify each entry
			if not t(v) then return false end
		end
		return true
	end
end
function expect.eswitch(cases)
	return function(m)
		local case, o = next(m)
		if not case or next(m, case) or type(case) ~= "string" then return false end
		if not cases[case] or not cases[case](o) then return false end
		m[case] = nil
		m.data = o
		m.case = case
		return true
	end
end
function expect.estr(min, max)
	return function(str) return type(str) == "string" and #str >= min and #str <= max end
end
function expect.eint(min, max)
	return function(int) return type(int) == "number" and int >= min and int <= max and math.floor(int) == int end
end
function expect.ebool(b)
	return not b or type(b) == "boolean"
end
function expect.eor(a, b)
	return function(n) return a(n) or b(n) end
end
function expect.eand(a, b)
	return function(n) return a(n) and b(n) end
end
local function makeOpt(base)
	return function(...)
		local t = base(...)
		return function(item) return item == nil or t(item) end
	end
end
expect.omap = makeOpt(expect.emap)
expect.oarr = makeOpt(expect.earr)
expect.oint = makeOpt(expect.eint)
expect.ostr = makeOpt(expect.estr)

setmetatable(expect, { __call = function(self, exp)
	return function(req, res, ...)
		local tags = { ... }
		if exp.query then
			req.uri.query["nil"] = nil
			local ok = exp.query(req.uri.query)
			if not ok then return res:error(400, "Query string error") end
			req.query = req.uri.query
		end
		if exp.json then
			util.getJson(req, res, function(json)
				local ok = exp.json(json)
				if not ok then return res:error(400, "Post body error") end
				exp[1](req, res, json, unpack(tags))
			end)
		else
			exp[1](req, res, unpack(tags))
		end
	end
end})

return expect

