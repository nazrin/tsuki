#include <lua.h>
#include <lauxlib.h>

#if LUA_VERSION_NUM >= 502
#define lua_objlen lua_rawlen
#endif

static int nilunpack(lua_State* L){
	luaL_argcheck(L, lua_istable(L, 1), 1, "Not table");
	luaL_argcheck(L, lua_isnone(L, 2), 2, "Extra args");
	size_t len = lua_objlen(L, 1);
	for(size_t i = 0; i < len; i++){
		lua_rawgeti(L, 1, i+1);
		if(lua_iscfunction(L, -1)){
			lua_pop(L, 1);
			lua_pushnil(L);
		}
	}
	return len;
}

int luaopen_src_nilunpack(lua_State* L){
	lua_newtable(L);
	lua_pushcfunction(L, &nilunpack);
	lua_setfield(L, -2, "unpack");
	lua_pushcfunction(L, &nilunpack);
	lua_setfield(L, -2, "NIL");
	return 1;
}

