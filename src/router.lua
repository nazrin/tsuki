local http = require("http")
local url = require("url")
local util = require("./src/util")
local config = require("./config")
local doRequest = require("./doreq")
local db = require("./db")
local uiHtml = require("./lib/ui-html/init")
local uiHtmlPath = "views/ui/"
uiHtml.set_path(uiHtmlPath)

local Router = { ADMIN = 100, MOD = 50, JANNY = 25 }
Router.__index = Router

local Route = {}
Route.__index = Route

function Route:cache(func)
	self.etagFunc = func
	return self
end
function Route:auth(priv)
	self.privRequired = priv
	return self
end

function Router:makeListen(typ, pat, cb)
	local r = { pat = "^"..pat.."$", cb = cb }
	table.insert(self[typ], r)
	return setmetatable(r, Route)
end
function Router:get(...)
	return self:makeListen("GET", ...)
end
function Router:post(...)
	return self:makeListen("POST", ...)
end

function Router:route(method, path)
	local m = self[method]
	if not m then return end
	for i,c in ipairs(self[method]) do
		local tags = { path:match(c.pat) }
		if tags[1] then
			return c, tags
		end
	end
end

-- Utilities for req, res, and render
local function injectUtils(req, res)
	res.req = req
	-- Res
	function res.render(view, vars)
		vars.tsuki = {
			config = config,
			util = util
		}
		vars.csp_nonce = res.csp_nonce
		vars.ui = uiHtml.render
		vars.uiTemplate = uiHtml.jsTemplate
		local status, err = pcall(function() doRequest.render(req, res, view, vars) end)
		if(err) then
			p(err)
			res:error(500, "Error in template.")
		end
	end
	function res.json(tbl)
		doRequest.json(req, res, tbl)
	end
	function res:error(code, str)
		print("Got error", code, str, req.url)
		-- print(debug.traceback())
		res.statusCode = code or 400
		res:finish(("Err %d: %s\n"):format(code or 400, str or ""))
	end
	-- Session
	local cookie = req.headers.Cookie
	if cookie then
		for k,v in cookie:gmatch("(%w+)=(%w+)") do
			if k == "auth" then
				local ses = db.getSession(v)
				if not ses then return res:error(400, "Invalid session") end
				req.session = ses
			end
		end
	end
	if not req.session then req.session = config.defaultSession end
end

function Router:start(bind)
	http.createServer(function(req, res)
		local uri = url.parse(req.url, true)
		req.uri = uri
		local c, tags = self:route(req.method, uri.pathname)
		if not c then return res:error(404) end
		if c.privRequired then
			if req.session.priv < c.privRequired then
				return res:error(401)
			end
		end
		if c.etagFunc then
			c.etagFunc(req, res, table.unpack(tags))
			if res.etag == req.headers["If-None-Match"] then
				res.statusCode = 304
				return res:finish()
			end
		end
		if config.proxied and req.headers["X-Forwarded-For"] then
			req.ip = req.headers["X-Forwarded-For"]
		else
			req.ip = req.socket:address().ip
		end
		res.csp_nonce = util.getRandomHex(16)
		injectUtils(req, res)
		c.cb(req, res, table.unpack(tags))
	end):listen(bind.port, bind.addr)
end

setmetatable(Router, { __call = function(self)
	local r = { GET = {}, POST = {} }
	return setmetatable(r, Router)
end })

local router = Router()

return Router
