local util = require("./src/util")
local config = require("./config")
local dbi = require("DBI")
local nilunpack = require("src.nilunpack")
local lil = require("lil")
local osshhash = require("openssh-hash")

local db = assert(dbi.Connect(nilunpack.unpack(config.db)))
db:autocommit(true)
local dbt = { boards = {} }

local function exec(cmd)
	local stmt = assert(db:prepare(cmd))
	assert(stmt:execute())
end

local Stmt = { args = {} }
Stmt.args.public = "id,thread,subject,name,email,content,date,flags,files"
Stmt.__index = Stmt
setmetatable(Stmt, { __call = function(self, cmd)
	cmd = util.template(cmd, Stmt.args)
	local named, done = {}, {}
	for m in cmd:gmatch(":([a-z]+)") do
		if not done[m] then
			table.insert(named, m)
			done[m] = true
		end
	end
	return setmetatable({
		s = assert(db:prepare(cmd)),
		named = named
	}, Stmt)
end })

function Stmt:exec(tbl)
	assert(self.s:execute(self:parseArgs(tbl)))
end
function Stmt:rows(tbl)
	self:exec(tbl)
	local rows = {}
	local names = self.s:columns()
	for raw in self.s:rows() do
		local row = {}
		table.insert(rows, row)
		for n,name in ipairs(names) do
			row[name] = raw[n]
		end
	end
	return rows
end
function Stmt:row(tbl)
	self:exec(tbl)
	local names = self.s:columns()
	return self.s:fetch(names)
end
function Stmt:parseArgs(args)
	if not args then return end
	local a = {}
	for index,name in ipairs(self.named) do
		if args[name] == nil then
			a[index] = nilunpack.NIL
		else
			a[index] = args[name]
		end
	end
	return nilunpack.unpack(a)
end

exec[[
	CREATE TABLE IF NOT EXISTS members (
		name   TEXT    NOT NULL UNIQUE,
		role   TEXT    NOT NULL,
		pass   BLOB    NOT NULL
	);
]]
exec[[
	CREATE TABLE IF NOT EXISTS member_tokens (
		token  TEXT    NOT NULL UNIQUE,
		date   INTEGER NOT NULL,
		user   TEXT    NOT NULL
	);
]]
local tokenList, memberList = {}, {}
local function applyPriv(m)
	if m.role == "admin" then
		m.priv = 100
	elseif m.role == "mod" then
		m.priv = 50
	elseif m.role == "janny" then
		m.priv = 25
	else
		m.priv = 0
	end
end
do
	local ml = assert(Stmt("SELECT name, role, pass FROM members"):rows())
	local tl = assert(Stmt("SELECT * FROM member_tokens"):rows())
	for i,m in ipairs(ml) do
		memberList[m.name] = m
		applyPriv(m)
	end
	for i,t in ipairs(tl) do
		tokenList[t.token] = memberList[t.user]
	end
end

-- Remember to salt your spaghetti before you boil it
local passSalt
do
	local exists, err = fs.existsSync(config.saltPath)
	assert(not err or err:match("^ENOENT:"), err)
	if exists then
		passSalt = assert(assert(io.open(config.saltPath)):read(32))
	else
		passSalt = util.getRandomBytes(32)
		io.open(config.saltPath, "w"):write(passSalt):close()
	end
end

local tokenaddStmt = Stmt("INSERT INTO member_tokens (token, date, user) VALUES (:token, :date, :user)")
function dbt.login(data, cb)
	local member = memberList[data.name]
	if not member          then return cb(false, "Wrong username or password") end
	local cipher = osshhash.cipher("SHA256")
	cipher:update(passSalt, data.pass)
	local hash = cipher:digest("string")
	if member.pass ~= hash then return cb(false, "Wrong username or password") end
	local tokenBlob = util.getRandomHex(32)
	tokenaddStmt:exec({ token = tokenBlob, date = os.time(), user = data.name })
	tokenList[tokenBlob] = memberList[data.name]
	cb(tokenBlob)
end
local useraddStmt = Stmt("INSERT INTO members (name, role, pass) VALUES (:name, :role, :pass)")
function dbt.useradd(data)
	local cipher = osshhash.cipher("SHA256")
	cipher:update(passSalt, data.pass)
	local hash = cipher:digest("string")
	local m = { name = data.name, role = data.role, pass = hash }
	local ok = pcall(useraddStmt.exec, useraddStmt, m)
	if ok then
		applyPriv(m)
		memberList[data.name] = m
	end
end
function dbt.getSession(token)
	return tokenList[token]
end

for i,board in ipairs(config.boards) do
	local tag = board.tag
	Stmt.args.tag = tag
	-- Files
	fs.mkdirSync(config.filePath)
	fs.mkdirSync(path.join(config.filePath, tag))
	-- Db
	local function doExec(cmd) exec(util.template(cmd, { tag = tag })) end
	doExec([[
		CREATE TABLE IF NOT EXISTS {{tag}}_posts (
			id      INTEGER NOT NULL UNIQUE,
			thread  INTEGER,
			subject TEXT,
			name    TEXT,
			email   TEXT,
			content TEXT,
			pass    TEXT,
			ip      TEXT,
			files   TEXT,
			date    INTEGER,
			bump    INTEGER,
			flags   INTEGER DEFAULT(0),
			PRIMARY KEY(id AUTOINCREMENT)
		);
	]])
	doExec([[
		CREATE TRIGGER IF NOT EXISTS updateBump_{{tag}} AFTER INSERT ON {{tag}}_posts BEGIN
			UPDATE {{tag}}_posts SET bump = NEW.date WHERE (CASE WHEN NEW.thread IS NULL THEN id == NEW.id ELSE id == NEW.thread END);
		END;
	]])
	local stmts = {
		checkIfThreadExists = Stmt("SELECT id FROM {{tag}}_posts WHERE    id == :id AND thread IS NULL"),
		getNewestPost       = Stmt("SELECT id FROM {{tag}}_posts WHERE ROWID == last_insert_rowid()"),
		createPost = Stmt([[
			INSERT INTO {{tag}}_posts (
				thread,  subject,  name,  email,  content,  pass,  ip,  date,  flags, files
			) VALUES (
				:thread, :subject, :name, :email, :content, :pass, :ip, :date, :flags, :files
			);
		]]),
		deletePost      = Stmt("DELETE FROM {{tag}}_posts WHERE (id == :id) OR (thread == :id)"),
		getPost         = Stmt("SELECT {{public}} FROM {{tag}}_posts WHERE (id == :id)"),
		getThread       = Stmt("SELECT {{public}} FROM {{tag}}_posts WHERE (id == :id OR thread == :id)"),
		getThreadSince  = Stmt("SELECT {{public}} FROM {{tag}}_posts WHERE (id == :id OR thread == :id) AND date >= :since"),
		getIndex        = Stmt("SELECT id FROM {{tag}}_posts WHERE THREAD IS NULL ORDER BY bump DESC LIMIT :limit OFFSET :offset"),
	}
	do
		local chunks = {}
		for i=1,5 do
			local c = string.char(65 + 32 + i - 1)
			chunks[i] = ([[
				SELECT {{public}},bump FROM (
					SELECT * FROM {{tag}}_posts
					WHERE thread IS NULL AND id == :%s
					ORDER BY bump DESC
				) UNION SELECT {{public}},bump FROM (
					SELECT * FROM {{tag}}_posts
					WHERE thread == :%s
					ORDER BY id DESC
					LIMIT 3
				)
			]]):format(c, c)
		end
		stmts.getIndexReplies = Stmt(table.concat(chunks, " UNION "))
	end
	-- API
	local function lastPostUpdate()
		dbt.boards[tag].stats.lastPostUpdate = os.date() .. " " .. util.getRandomHex(6)
	end
	dbt.boards[tag] = {
		post = function(data, cb)
			local function fail(id, reason)
				if id then
					-- Clean up DB and folder goes here
				end
				cb(false, reason)
			end
			if (not data.content) and (not data.files) then
				return fail(nil, "No content/files given")
			end
			data.date = os.time()
			local files = data.files
			local filesJson = {}
			if files then
				for f,file in ipairs(files) do
					if file.name:find("/") then
						return fail(false, "Invalid filename")
					end
					file.mime = util.getType(file.name) or "?"
					file.ext = file.name:match("[^.]+$")
					filesJson[f] = { name = file.name, size = #file.data, mime = file.mime }
					local ok, img = pcall(lil.import, file.data)
					if ok and img then
						local nw, nh
						local rw, rh = unpack(config.thumbnails.size)
						if img.w > img.h then
							if img.w <= rw then
								nw, nh = img.w, img.h
							else
								nw, nh = rw, (img.h / img.w) * rh
							end
						else
							if img.h <= rh then
								nw, nh = img.w, img.h
							else
								nw, nh = (img.w / img.h) * rw, rh
							end
						end
						nw = math.max(1, math.floor(nw))
						nh = math.max(1, math.floor(nh))
						file.img = img
						file.res  = { img.w, img.h }
						file.tres = { nw, nh }
						filesJson[f].res  = file.res
						filesJson[f].tres = file.tres
					end
				end
				data.files = json.encode(filesJson)
			else
				data.files = nil
			end
			if data.thread then
				if not stmts.checkIfThreadExists:rows({ id = data.thread }) then return fail(false, "Thread does not exist") end
			end
			stmts.createPost:exec(data)
			local post = stmts.getNewestPost:row()
			if not post then return fail(post.id, "?") end
			lastPostUpdate()
			local threadId = data.thread or post.id
			local filePath  = path.join(config.filePath, tag, ("%d"):format(threadId))
			local function go()
				if files then
					local done = 0
					local function canFinish(err)
						if err then done = -1 return fail(post.id, err) end
						done = done + 1
						if done == #files * 2 then
							cb(post)
						end
					end
					for f,file in ipairs(files) do
						local pa = path.join(filePath, ("%d-%d.%s"):format(post.id, f, file.ext))
						local pt = path.join(filePath, ("%d-%ds%s"):format(post.id, f, config.thumbnails.format))
						fs.writeFile(pa, file.data, canFinish)
						if file.img then
							local tdata = file.img:resize(file.tres[1], file.tres[2]):export(config.thumbnails.format, config.thumbnails.ops)
							fs.writeFile(pt, tdata, canFinish)
						else
							canFinish()
						end
					end
				else
					cb(post)
				end
			end
			if not data.thread then
				fs.mkdir(filePath, function(err)
					if err then return fail(post.id, err) end
					go()
				end)
			else
				go()
			end
		end,
		getIndex = function(data, cb)
			local page = data.page or 1
			local countThreads = data.threads or 5
			local countReplies = data.replies or 5
			local threads = stmts.getIndex:rows({ limit = countThreads, offset = countThreads * (page-1) })
			if not threads then return cb(false, "No threads") end
			local ids = {}
			for t,thread in ipairs(threads) do
				ids[string.char(65 + 32 + t - 1)] = thread.id
			end
			for t=1,5 - #threads do
				ids[string.char(65 + 32 + t - 1 + #threads)] = 0
			end
			local d = stmts.getIndexReplies:rows(ids)
			cb(d)
		end,
		getThread = function(data, cb)
			local posts
			if data.since then
				posts = stmts.getThreadSince:rows(data)
			else
				posts = stmts.getThread:rows(data)
			end
			cb(posts)
		end,
		getPost = function(data, cb)
			local post = stmts.getPost:row(data)
			cb(post)
		end,
		deletePost = function(data, cb)
			stmts.deletePost:exec(data)
			lastPostUpdate()
			cb(true)
		end,
	}
	-- Stats
	dbt.boards[tag].stats = {
		lastPostUpdate = os.date()
	}
end

return dbt
