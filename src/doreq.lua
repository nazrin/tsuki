local joinPath = require("path").join
local config = require("./config")
local util = require("./src/util")
local lte = require("./lib/lte/init")

lte.set_view_path("./views")

local doRequest = {}

setmetatable(doRequest, { __call = function(self, req, res, props)
	-- Type
	if props.type then
		res:setHeader("Content-Type", props.type)
	elseif props.path then
		res:setHeader("Content-Type", util.getType(props.path))
	else
		res:setHeader("Content-Type", "text/plain")
	end
	-- Misc
	if res.etag then
		res:setHeader("ETag", res.etag)
		res:setHeader("Cache-Control", "public")
	end
	if res.maxAge then
		res:setHeader("Cache-Control", ("public, max-age=%d"):format(res.maxAge))
	end
	if res.cookies then
		for c,cookie in pairs(res.cookies) do
			res:setHeader("Set-Cookie", ("%s=%s"):format(c, cookie))
		end
	end
	
	-- CSP
	local defaultSrc = "default-src 'self'"
	local scriptSrc = ("script-src 'nonce-%s' 'unsafe-eval'"):format(res.csp_nonce)
	local styleSrc = ("style-src 'nonce-%s'"):format(res.csp_nonce)
	local objectSrc = "object-src 'none'"
	res:setHeader("Content-Security-Policy", table.concat({defaultSrc, scriptSrc, styleSrc, objectSrc}, "; "))
	
	-- Content
	if props.body then
		res:setHeader("Content-Length", #props.body)
		res:finish(props.body)
	elseif props.path then
		res:setHeader("Content-Length", props.size)
		fs.createReadStream(props.path):pipe(res)
	else
		res:setHeader("Content-Length", 0)
		res:finish("")
	end
end })

local function getErr(str)
	if str:match("^ENOENT") then
		return 404
	end
	return 500
end

function doRequest.json(req, res, tbl)
	doRequest(req, res, {
		body = json.encode(tbl),
		type = "application/json"
	})
end
function doRequest.render(req, res, ...)
	local html = lte.render(...)
	doRequest(req, res, {
		type = "text/html", body = html
	})
end
function doRequest.public(req, res, path)
	if path:find("%.%.") then
		return res:error(400, "Invalid URL")
	end
	path = joinPath(config.publicRoot, path)
	fs.stat(path, function(err, stat)
		if err then return res:error(getErr(err)) end
		if stat.type ~= "file" then
			path = joinPath(path, "index.html")
			fs.stat(path, function(err, stat)
				if err then return res:error(getErr(err)) end
				doRequest(req, res, { path = path, size = stat.size })
			end)
		else
			doRequest(req, res, { path = path, size = stat.size })
		end
	end)
end

return doRequest
