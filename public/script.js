// Utils
(function(){
	let utils = {};
	window.utils = utils;
	
	utils.round = function(n, places){
		return Math.round(n * (10**places)) / (10**places)
	};
	
	let divideBytes = 1000 
	let bytesArray = ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"]
	utils.humanReadableBytes = function(bytes, divide=divideBytes){
		let counter = 0;
		
		while(bytes > divideBytes){
			bytes = bytes / divideBytes;
			counter++;
		}
		
		return utils.round(bytes, 2) + " " + bytesArray[counter];
	};
	
	utils.date = function(timestamp){
		let date = new Date(timestamp);
		
		let jp = date.toLocaleString("jp", {timeZone: serverTimezone});
		let [_, d, m, y, time] = jp.match(/^(.+)\/(.+)\/(.+), (.+)$/);
		return `${d}/${m}/${y} ${time}`;
	}
	
	let div = document.createElement("div");
	utils.htmlEscape = function(html){
		div.textContent = html;
		return div.innerHTML;
	}
	
	utils.url = {};
	utils.url.thread = function(board, thread){
		return `/${board}/${thread}`;
	}

})();

// Api
(function(){
	localStorage.myPass = "strongpassword123"; // TODO XXX
	class Api{

		get(url, data){
			return this.call("GET", url, data);
		}
		post(url, data){
			return this.call("POST", url, data);
		}
		call(method, url, data){
			return new Promise(async (success, error) => {
				let xhr = new XMLHttpRequest();
				if(method == "GET"){
					let d = [];
					for(let i in data){
						d.push(`${i}=${data[i]}`);
					}
					url += "?" + d.join("&");
					data = null;
				}
				
				xhr.open(method||"GET", url);
				xhr.setRequestHeader("Content-Type", "application/msgpack");
				xhr.onreadystatechange = function(){
					if(this.readyState == 4){
						let response = this.responseText;
						try{
							response = JSON.parse(response);
						}catch(e){}
						let data = {
							data: response,
							xhr: this
						};
						if(this.status == 200){
							success(data);
						}else{
							error(data);
						}
					}
				}

				if(data instanceof HTMLFormElement){
					let object = {};
					let inputs = data.querySelectorAll("input[name], textarea[name]");
					for(let input of inputs){
						if(input.type != "file"){
							let dbType = input.getAttribute("db-type");
							switch(dbType){
								case "int": {
									object[input.name] = +input.value;
									break;
								}
								default: {
									if(input.value != "")
										object[input.name] = input.value;
								}
							}
						}else if(input.files.length){
							if(!object.files) object.files = [];
							for(let file of input.files){
								let buffer = await new Promise((success, error) => {
									let reader = new FileReader();
									reader.onload = function(){
										success(reader.result);
									};
									reader.readAsArrayBuffer(file);
								});
								object.files.push({
									name: file.name,
									data: new Uint8Array(buffer)
								});
							}
						}
					}
					data = object;
				}
				if(data != null){
					data.pass = localStorage.myPass;
					data = msgpack.serialize(data);
				}
				xhr.send(data);
			});
		}
	}

	window.api = new Api();
})();

// Api Forms
(function(){
	let apiForms = document.querySelectorAll("form[api]");
	apiForms.forEach(apiForm => {
		let inputs = apiForm.querySelectorAll('input[type="text"], input[type="file"], textarea');
		apiForm.addEventListener("submit", e => {
			e.stopPropagation();
			e.preventDefault();

			let method = apiForm.getAttribute("method");
			let action = apiForm.getAttribute("action");
			api.call(method, action, apiForm).then(response => {
				inputs.forEach(input => {
					input.value = "";
				});
				
				let event = new CustomEvent("api-response", {bubbles: true});
				event.response = response;
				apiForm.dispatchEvent(event);
				
				let popupForm = apiForm.closest("#popup-form");
				if(popupForm) popupForm.classList.remove("show");
			}).catch(error => {
				console.log(error);
			});
		});
	});
})();

// Gallery
(function(){
	let gallery = document.getElementById('gallery');
	let galleryNumber = document.getElementById('gallery-info-number');
	let galleryText = document.getElementById('gallery-info-text');
	let galleryClose = document.getElementById('gallery-info-close');
	let galleryPost = document.getElementById('gallery-post');
	let galleryMediaWrapper = document.getElementById('gallery-media-wrapper');
	let galleryMedia = document.getElementById('gallery-media');
	let galleryMediaBack = document.getElementById('gallery-media-back');
	let galleryMediaForward = document.getElementById('gallery-media-forward');
	let galleryImg = new Image();
	galleryMedia.appendChild(galleryImg);
	let galleryLinks = [];
	let galleryIndex = 0;
	let currLink = null;
	let currPost = null;
	
	window.addEventListener('click', e => {
		let thumbLink = e.target.closest('#main .thumb-link');
		if(!thumbLink) return;
		e.stopPropagation();
		e.preventDefault();
		
		galleryLinks = [...document.querySelectorAll('#main .thumb-link')];
		galleryIndex = galleryLinks.indexOf(thumbLink);

		gallerySwitchTo(thumbLink);
	});
	
	function gallerySwitchToNext(){
		galleryIndex++;
		if(galleryIndex >= galleryLinks.length) galleryIndex = galleryLinks.length - 1;
		gallerySwitchTo(galleryLinks[galleryIndex]);
	}
	function gallerySwitchToPrev(){
		galleryIndex--;
		if(galleryIndex < 0) galleryIndex = 0;
		gallerySwitchTo(galleryLinks[galleryIndex]);
	}
	function gallerySwitchTo(thumbLink){
		if(thumbLink == currLink && gallery.classList.contains('show')) return;
		currLink = thumbLink;
		
		gallery.classList.remove('first', 'last');
		if(galleryIndex == 0){
			gallery.classList.add('first');
		}else if(galleryIndex == galleryLinks.length - 1){
			gallery.classList.add('last');
		}
		
		let post = thumbLink.closest('.post');
		if(post != currPost){
			let clonePost = post.cloneNode(true);
			clonePost.addEventListener('click', e => {
				let thumbLink = e.target.closest('.thumb-link');
				if(!thumbLink) return;
				e.stopPropagation();
				e.preventDefault();
				
				let wrapper = thumbLink.parentNode
				let index = [...wrapper.parentNode.children].indexOf(wrapper);
				currPost.querySelectorAll('.thumb-link')[index].click();
			});
			galleryPost.innerHTML = "";
			galleryPost.appendChild(clonePost);
			galleryPost.scrollTop = 0;
		}
		currPost = post;
		
		galleryNumber.textContent = (galleryIndex+1) + " of " + galleryLinks.length;
		
		let img = thumbLink.firstElementChild;
		// Empty attributes first to prevent user from seeing image switching
		if(!gallery.classList.contains('show')){
			galleryImg.src = "";
			galleryImg.alt = "";
		}
		// Switch to image attributes
		galleryImg.src = thumbLink.href;
		galleryImg.alt = img.alt;
		galleryText.textContent = img.alt;
		// Show the gallery
		gallery.classList.add('show');
		// Scroll to post
		thumbLink.closest('.post');
		post.scrollIntoView();
	}
	
	window.addEventListener('keydown', e => {
		if(!gallery.classList.contains('show')) return;
		switch(e.key){
			case "Escape": {
				gallery.classList.remove('show');
				break;
			}
			case "ArrowRight": {
				gallerySwitchToNext();
				break;
			}
			case "ArrowLeft": {
				gallerySwitchToPrev();
				break;
			}
			default: return;
		}
		e.stopPropagation();
		e.preventDefault();
	}, true);
	
	gallery.addEventListener('click', e => {
		if(e.target != gallery) return;
		gallery.classList.remove('show');
	});
	galleryClose.addEventListener('click', e => {
		gallery.classList.remove('show');
	});
	galleryMedia.addEventListener('mousedown', e => {
		e.preventDefault();
		e.stopPropagation();
	});
	galleryMediaBack.addEventListener('click', gallerySwitchToPrev);
	galleryMediaForward.addEventListener('click', gallerySwitchToNext);
	galleryMediaBack.addEventListener('mousedown', e => {
		e.stopPropagation();
		e.preventDefault();
	});
	galleryMediaForward.addEventListener('mousedown', e => {
		e.stopPropagation();
		e.preventDefault();
	});
	galleryMediaWrapper.addEventListener('click', e => {
		if(e.target != galleryMediaWrapper && e.target != galleryMedia) return;
		gallery.classList.remove('show');
	});
})();

// Popup reply
(function(){
	let popupForm = document.getElementById('popup-form');
	if(!popupForm) return;
	let header = document.querySelector('.popup-form-header');
	let close = document.querySelector('.popup-form-close');
	let popupFormTextarea = document.querySelector('.popup-form textarea');
	
	let dragging = false;
	header.addEventListener('mousedown', e => {
		if(e.target.closest('.popup-form-close')) return;
		e.preventDefault();
		e.stopPropagation();
		let rects = popupForm.getBoundingClientRect();
		dragging = {x: e.clientX - rects.x, y: e.clientY - rects.y};
	});
	window.addEventListener('mousemove', e => {
		if(!dragging) return;
		popupForm.style.left = (e.clientX - dragging.x) + "px";
		popupForm.style.top = (e.clientY - dragging.y) + "px";
	});
	window.addEventListener('mouseup', e => {
		dragging = false;
	});
	close.addEventListener('click', e => {
		popupForm.classList.remove('show');
	});
	window.addEventListener('click', e => {
		let postIdNumber = e.target.classList.contains('post-id-number');
		let href = e.target.getAttribute('href') == "#popup-form";
		if(!postIdNumber && !href) return;
		e.preventDefault();
		e.stopPropagation();
		
		if(postIdNumber) popupFormTextarea.value += `>>${e.target.textContent}\n`;
		popupForm.classList.add('show');
	});
})();
