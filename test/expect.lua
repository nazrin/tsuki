-- This is so hacky and retarded but it works
local json, failed

package.loaded["./src/util"] = {
	getJson = function(req, res, cb)
		cb(json)
	end
}

local expect = require("../src/expect.lua")
local eint, estr, earr, emap = expect.eint, expect.estr, expect.earr, expect.emap
local oint, ostr, oarr, omap = expect.oint, expect.ostr, expect.oarr, expect.omap
local eand, eor = expect.eand, expect.eor

local total = 0
local function texpect(j, t)
	failed = nil
	local exp = {
		json = j
	}
	exp[1] = function() end
	local e = expect(exp)
	json = t
	e(nil, { error = function(self, code, err) failed = err end })
	total = total + 1
end
local function testExpect(...)
	texpect(...)
	if failed then error("Failed", 2) end
end
local function testExpectFail(...)
	texpect(...)
	if not failed then error("Didn't fail", 2) end
end

local iceCream = emap{
	ice = emap{ cream = estr(1, 5) }
}
testExpect(iceCream, { ice = { cream = "yum!" } })
testExpectFail(iceCream, { ice = { cream = "yum!", berg = "stein" } })
testExpectFail(iceCream, { ice = {} })
testExpectFail(iceCream, { ice = { cream = "yummy!" } })
testExpectFail(iceCream, { ice = { cream = 5 } })
testExpectFail(iceCream, { ice = { cream = {} } })

local fileList = emap{
	files = earr(1, 2, emap{
		data = estr(0, 16), name = estr(1, 16)
	})
}
testExpect(fileList, { files = { { data = "hi", name = "hello.txt" } } })
testExpectFail(fileList, { files = { cream = "yum!" } })
testExpectFail(fileList, { files = { { data = "hi", "hello.txt" } } })
testExpectFail(fileList, {})
testExpectFail(fileList, { files = { { data = "1", name = "1" }, { data = "2", name = "2" }, { data = "3", name = "3" } } })

local iceCreamFileList = eor(fileList, iceCream)
testExpect(iceCreamFileList, { ice = { cream = "yum!" } })
testExpect(iceCreamFileList, { files = { { data = "hi", name = "hello.txt" } } })

print(("%d tests passed"):format(total))

