_G.json = require("json")
_G.fs = require("fs")
_G.path = require("path")

if not fs.existsSync("./config.lua") then
	if fs.existsSync("./config.def.lua") then
		io.open("./config.lua", "w"):write(io.open("./config.def.lua"):read("*a"))
	else
		error("Where are the configs?")
	end
end

local util = require("./src/util.lua")
local config = require("./config")
local Router = require("./src/router")
local expect = require("./src/expect")
local db = require("./src/db")
local doRequest = require("./src/doreq")

local eint, estr, earr, emap, ebool = expect.eint, expect.estr, expect.earr, expect.emap, expect.ebool
local oint, ostr, oarr, omap        = expect.oint, expect.ostr, expect.oarr, expect.omap
local eand, eor                     = expect.eand, expect.eor

local router = Router()

for i,board in ipairs(config.boards) do
	config.boards[board.tag] = board
end

local serverStartEtag = ("%x"):format(os.time())


-- API POST --
router:post("/api/board/(%w+)/post/?", expect({
	json = emap{
		thread  = oint(1, 2^45),
		subject = ostr(1, 512),
		name    = ostr(1, 128),
		email   = ostr(1, 128),
		content = ostr(1, 8192),
		pass    = estr(16, 32),
		files = oarr(1, config.maxFileNum, emap{
			name = estr(1, 128),
			data = estr(1, config.maxFileSize),
		})
	},
	function(req, res, json, boardTag)
		local board = db.boards[boardTag]
		if not board then return res:error(404, "No such board") end
		json.ip = req.ip
		board.post(json, function(d, err)
			if not d then return res:error(400, err) end
			doRequest.json(req, res, d)
		end)
	end
}))
router:post("/api/login/?", expect({
	json = emap{
		name = estr(5, 32),
		pass = estr(4, 256),
	},
	function(req, res, json)
		db.login(json, function(token, err)
			if not token then return res:error(400, err) end
			res.cookies = { auth = token }
			doRequest(req, res, {})
		end)
	end
}))

-- API POST ADMIN --
router:post("/api/admin/board/(%w+)/deletePost/?", expect({
	json = emap{
		id     = eint(1, 2^45),
		reason = ostr(1, 8192),
		byIp   = ebool,
	},
	function(req, res, json, boardTag)
		local board = db.boards[boardTag]
		if not board then return res:error(404, "No such board") end
		board.deletePost(json, function()
			doRequest(req, res, {})
		end)
	end
})):auth(Router.MOD)

-- API GET --
router:get("/api/board/(%w+)/index/?", function(req, res, boardTag)
	local board = db.boards[boardTag]
	if not board then return res:error(404, "No such board") end
	local index = board.getIndex({ page = 1, threads = 5, replies = 5 }, function(d, err)
		if not d then return res:error(400, err) end
		res.json(d)
	end)
end)

router:get("/api/board/(%w+)/(%d+)/?", expect({
	query = expect.convmap{
		since = { tonumber, oint(1, math.huge) }
	},
	function(req, res, boardTag, threadId)
		local board = db.boards[boardTag]
		if not board then return res:error(404, "No such board") end
		local d = { id = threadId, since = req.query.since }
		board.getThread(d, function(posts)
			if not posts then return res:error(404, "No such thread") end
			res.json(posts)
		end)
	end
}))

-- GET --
router:get("/", function(req, res)
	res.render("index", { test = ":)" })
end):cache(function(req, res)
	res.etag = serverStartEtag
end)

local function buildThreads(configBoard, d)
	local index = {}
	local indexIndex = {}
	-- Assuming response gives thread followed its replies X times
	for i,post in ipairs(d) do
		post.subject = util.htmlEscape(post.subject)
		post.name = util.htmlEscape(post.name)
		post.content = util.htmlEscape(post.content)
		
		if post.files then
			post.files = json.decode(post.files)
			for i,file in ipairs(post.files) do
				local _i, _j, ext = file.name:find("%.([^%.]+)$")
				file.ext = ext
				
				file.thumb_url = ("/files/%s/%d/%d-%ds%s"):format(configBoard.tag, post.thread and post.thread or post.id, post.id, i, config.thumbnails.format)
				file.url = ("/files/%s/%d/%d-%d.%s"):format(configBoard.tag, post.thread and post.thread or post.id, post.id, i, file.ext)
				
				file.iw = file.tres[1]
				file.ih = file.tres[2]
				
				file.type = file.mime:match("^[^/]+")
				file.hSize = util.humanReadableBytes(file.size)
			end
		end
		if post.thread == nil then
			table.insert(index, post)
			indexIndex[post.id] = post
			post.replies = {}
		else
			table.insert(indexIndex[post.thread].replies, post)
		end
	end
	table.sort(index, function(a,b) return a.bump > b.bump end)
	return index, indexIndex
end
local function postUpdateCache(req, res, boardTag)
	local board = db.boards[boardTag]
	if not board then return res:error(404, "No such board") end
	res.etag = board.stats.lastPostUpdate
end
router:get("/(%w+)/?", function(req, res, boardTag)
	local configBoard = config.boards[boardTag]
	local board = db.boards[boardTag]
	if not board then return res:error(404, "No such board") end
	local index = board.getIndex({ page = 1, threads = 5, replies = 5 }, function(d, err)
		if not d then return res:error(400, err) end
		local index = buildThreads(configBoard, d)
		res.render("board", { board = configBoard, index = index })
	end)
end):cache(postUpdateCache)

router:get("/(%w+)/(%d+)/?", function(req, res, boardTag, threadId)
	local configBoard = config.boards[boardTag]
	local board = db.boards[boardTag]
	if not board then return res:error(404, "No such board") end
	local d = { id = threadId }
	board.getThread(d, function(posts)
		if not posts then return res:error(400, err) end
		local index, indexIndex = buildThreads(configBoard, posts)
		res.render("thread", { board = configBoard, thread = indexIndex[tonumber(threadId)] })
	end)
end):cache(postUpdateCache)

router:get("(/files/.*)", doRequest.public):cache(function(req, res)
	res.etag = serverStartEtag
	res.maxAge = config.publicMaxAge or 3600000
end)

router:get("(.*)", doRequest.public):cache(function(req, res)
	res.etag = serverStartEtag
end)

-- POST --
router:post("(.*)", function(req, res) res:error(404) end) -- Reject any other calls

print(("http://%s:%d"):format(config.bind.addr, config.bind.port))
router:start(config.bind)
